<?php

/**
 * Trait controllerTrait
 * @author Wilson Junior
 */
trait controllerTrait
{
    /**
     * @return array
     */
    protected function getRequestData()
    {
        $data = [];
        if (isset($_POST)) {
            foreach ($_POST as $key => $value) {
                if (!empty($value)) {
                    $data[$key] = $value;
                }
            }
        }

        return $data;
    }
}

?>