<?php
require_once 'model/Product.php';
require_once 'controller/controllerTrait.php';

/**
 * Class dashboardController
 * @author Wilson Junior
 */
class dashboardController
{
    use controllerTrait;
    /**
     * @var Product
     */
    private $product;

    /**
     * ClienteController constructor.
     */
    public function __construct()
    {
        $this->product = new Product();
    }

    public function index()
    {
        $page = $_GET["page"];
        if (!$page) {
            $page = 1;
        }
        $products = $this->product->listAll(true, $page, 10);
        $this->product->closeDBConnection();
        $pages = $products["pages"];
        if (!empty($pages)) {
            if (strpos($pages, ".") !== false) {
                $pages = (int) $pages + 1;
            }
            $_REQUEST['pages'] = $pages;
        }
        unset($products["pages"]);
        $_REQUEST['products'] = $products;

        require_once 'assets/dashboard.php';
    }
}