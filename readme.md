# Projeto

Para desenvolvimento do projeto utlizei as seguintes tecnologias:
 
**PHP 7.2** - Versão mais estavel do PHP.

 **NGINX 1.14.0** - Para servidor HTTP.
 
 **MySQL 14.14** - Para servidor de banco de dados.
 
 **PhPStorm 2019.1.3** - Para Ide de desenvolvimento.

### Configuração do Projeto

Acesse a pasta onde irá baixar o projeto e faça o download da branch "master":

 Como pasta utilizei a raiz "var/www/" 

https://bitbucket.org/woliveirajr/desafio/src/master/

Como iremos salvar arquivos e imagens deve ser alterada a permisão das 2 pastas que irão "gaurdar" esses arquivos
- Para a pasta de imagens usar o seguinte comando
    - sudo chmod -R 777 assets/images/
- Para a pasta de onde irão ficar os csv usar o seguinte comando
    - sudo chmod -R 777 files/
 

 
Agora devemores configura a conexão do banco de dados, para facilitar essa conexão foi criado o environment.php,
que facilmente pode ser alterado para apontar para outro banco.

##### Configuração do environment.php

 
    DATABASE_SERVER = "localhost"; // IP do servidor
    DATABASE_NAME = "phpTest"; // Nome do banco de dados
    DATABASE_USERNAME = "root"; // Login do banco
    DATABASE_PASSWORD = "vagrant"; // Senha do banco
    
 
Apos a configuração devemos criar o banco e as tabelas do sistema.

rodando os 2 comandos seguintes esses passos serão feitos.

##### Comandos

comand para criar o banco de dados:

    php -f /var/www/desafio/configs/comands/CreateDB.php
    
comand para criar as tabelas de dados:
    
    php -f /var/www/desafio/configs/comands/CreateDB.php
    
##### Observação: O Banco é criado conforme a configuração do environment.php


### Configuração do Server HTTP **NGINX**


Instalação: **apt-get install nginx**

Agora devemos acessar o sites-availables do **NGINX** e colar a configuração a seguir

    server {
            listen 80;
            server_name desafio.webjump.com.br;
            access_log /var/log/nginx/localhost_access.log;
            error_log /var/log/nginx/localhost_error.log;
            rewrite_log on;
            root /var/www/desafio;
            index index.php index.html;
            location / {
                    try_files $uri $uri/ /index.php?$args;
            }
            if (!-d $request_filename) {
             rewrite ^/(.+)/$ /$1 permanent;
            }
            location ~* \.php$ {
                    fastcgi_pass unix:/run/php/php7.2-fpm.sock;
                    fastcgi_index index.php;
                    fastcgi_split_path_info ^(.+\.php)(.*)$;
                    include /etc/nginx/fastcgi_params;
                    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                    fastcgi_read_timeout 300;
            }
            location ~ /\.ht {
                    deny all;
            }
    }

- Dados do NGINX que podem ser alterados
    - server_name desafio.webjump.com.br; **- deve ser preenchido com o host**
    - root /var/www/desafio; - **localização do arquivo index.php**
    
#### Configuração importamte para o funcionamento do Importar por CSV

Devido ao timeout da página, não foi possível fazer a importação dentro da página, então foi criado um cron para rodar a cada 5 minutos.

O cron verifica uma tabela onde no momento da "importação" salva os dados do arquivo que deve ser importado.

Para configuração deve ser executado o seguinte comando:

    crontab -e
    
 e adicionado o seguinte comando, o caminho deve ser conforme o caminho do cron dentro da configuração do projeto:

    */5 * * * * php -f  /var/www/desafio/configs/cron/cron.php
    
    
 ## Dados para contato
 
 Email: wjunior013@gmail.com
 
 Celular: (24) 97402-0536
 
 LinkedIn: https://www.linkedin.com/in/wilson-oliveira-65495299/