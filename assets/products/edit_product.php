<?php
$categories = $_REQUEST['categories'];
$product    = $_REQUEST['product'];
require_once 'assets/required/bootstrap.php';
?>
<style>
    <?php include 'assets/css/style.css';?>
</style>
<div id="mySidenav" class="sidenav text-center">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <a href="/category">Categorias</a>
    <a href="/product">Produtos</a>
    <img src='../assets/images/go-logo.png'>
</div>
<header>
    <title>Webjump | Backend Test | Dashboard</title>
    <div class="go-menu">
        <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
        <a href="dashboard.html" class="link-logo">
            <img src="../assets/images/go-logo.png" alt="Welcome" width="69" height="430"/></a>
    </div>
</header>
<body>
<div class='card mt-1'>
    <!--    <div class='card-header'><h2>Cadastrar Categoria</h2></div>-->
    <div class='card-body'>
        <h1 class="title new-item">Editar Produto</h1>
        <form enctype="multipart/form-data" action="../product/update" method="POST">
            <input type='text' name='id' id='id' value='<?php echo $product['product_id'] ?>' hidden>
            <div class='row'>
                <div class='form-group col-md-6'>
                    <label for='name'>Nome</label>
                    <input type='text' class='form-control' name='name' id='name' value='<?php echo $product['product_name'] ?>' required='required'>
                </div>
                <div class='form-group col-md-6'>
                    <label for='code'>SKU</label>
                    <input type='text' class='form-control' name='sku' id='sku' value='<?php echo $product['product_SKU'] ?>' required='required'>
                </div>
            </div>
            <div class='row'>
                <div class='form-group col-md-6'>
                    <label for='name'>Preço</label>
                    <input type='text' class='form-control money' name='price' id='price' value='<?php echo $product['product_price'] ?>' required='required'>
                </div>
                <div class='form-group col-md-6'>
                    <label for='name'>Quantidade</label>
                    <input type='text' class='form-control' name='quantity' id='quantity' value='<?php echo $product['product_quantity'] ?>' required='required'>
                </div>
            </div>
            <div class='row'>
                <div class='form-group col-md-6'>
                    <label for='code'>Descrição</label>
                    <textarea class='form-control' name='description' id='description'> <?php echo $product['product_description'] ?> </textarea>
                </div>
                <div class='form-group col-md-6'>
                    <label for='code'>Descrição</label>
                    <?php foreach ($product['categories'] as $productCategory) {
                        $productCategoryIds[] = $productCategory[0]['id'];
                    }
                    ?>
                    <select class="form-control categories" multiple name="categories[]" id="categories" required='required'>
                        <?php foreach ($categories as $category): ?>
                            <option value='<?php echo $category['id'] ?>'<?php if(in_array($category['id'],$productCategoryIds))
                                echo "selected"?>
                            > <?php echo $category['name'] ?> </option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class='row'>
                <div class='form-group col-md-6'>
                    <label for='image'>Selecione uma imagem:</label>
                    <input name="image" id="image" type="file"/>
                    <?php if (empty($product['product_image_path'])): ?>
                        <img id="thumbnail" src="#" style='
    min-width: 500px;
    max-width: 500px;' required='required'>
                    <?php else: ?>
                        <img id="thumbnail" src="<?php echo $product['product_image_path'] ?>" style='
    min-width: 500px;
    max-width: 500px;' required='required'>
                    <?php endif ?>
                </div>
            </div>
            <input type='hidden' hidden name='active_flag' id='active_flag' value='1'>
            <button type='submit' class='btn btn-success '>Atualizar</button>
        </form>
    </div>
</div>
</body>
<script>
    <?php include 'assets/required/scripts.js'?>
    $(document).on("change", "#image", function (e) {
        showThumbnail(this.files);
    });

    function showThumbnail(files) {
        if (files && files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                $('#thumbnail').attr('src', e.target.result);
            }
            reader.readAsDataURL(files[0]);
        }
    }
    $('.categories').select2({placeholder: 'Selecione um item'});
    $('.money').mask('#.##0,00', {reverse: true});
</script>
<footer>
    <div class="footer-image">
        <img src="../assets/images/go-jumpers.png" width="119" height="26" alt="Go Jumpers"/>
    </div>
    <div class="email-content">
        <span>wjunior013@gmail.com</span>
    </div>
</footer>