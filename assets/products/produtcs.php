<?php
$products = $_REQUEST['products'];
$pages    = $_REQUEST['pages'];
$page     = $_GET["page"];
$url      = $_SERVER["HTTP_ORIGIN"];
if (!$page) {
    $page = 1;
}
if ($pages <= 0) {
    $page = 0;
}
require_once 'assets/required/bootstrap.php';
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<div id="mySidenav" class="sidenav text-center">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <a href="/category">Categorias</a>
    <a href="/product">Produtos</a>
    <img src='assets/images/go-logo.png'>
</div>
<header>
    <title>Webjump | Backend Test | Dashboard</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <div class="go-menu">
        <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
        <a href="dashboard.html" class="link-logo">
            <img src="assets/images/go-logo.png" alt="Welcome" width="69" height="430"/></a>
    </div>
</header>
<style>
    <?php include 'assets/css/style.css';?>
</style>
<body class='text-center'>
<div class='card mt-1'>
    <div class='card-body center'>
        <div witdh='100%' class='text-right'>
            <a href='product/create' class='btn btn-success'>+ Cadastrar Novo Produto</a>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                <i class="fas fa-file-csv"></i> Importar CSV
            </button>
        </div>
        <table class='table table-bordered table-hover mt-2'>
            <tr>
                <th>Imagem</th>
                <th>Nome</th>
                <th>SKU</th>
                <th>Descrição</th>
                <th>Categoria(s)</th>
                <th>Ações</th>
            </tr>
            <?php if (count($products) > 0): ?>
                <?php foreach ($products as $product): ?>
                    <tr class='text-center'>
                        <?php if (empty($product["product_image_path"])): ?>
                            <td><img class='productImage' src='<?php echo "assets/images/no-image.jpg"; ?>'></td>
                        <?php else: ?>
                            <td><img class='productImage' src='<?php echo $product["product_image_path"]; ?>'></td>
                        <?php endif; ?>
                        <td><?php echo $product["product_name"]; ?>
                            <br>
                            <?php echo "R$" . money_format('%i', $product["product_price"]); ?>
                        </td>
                        <td>
                            <?php echo $product["product_SKU"]; ?>
                        </td>
                        <td><?php echo $product["product_description"]; ?></td>
                        <td>
                            <ul>
                                <?php foreach ($product["categories"] as $category): ?>
                                    <li><?php echo $category[0]["name"]; ?></li>
                                <?php endforeach ?>
                            </ul>
                        </td>
                        <td>
                            <?php
                            $productId = $product['product_id'];
                            ?>
                            <?php if ($product['product_active_flag'] == 1) {
                                echo '<a href="product/delete?id=' . $productId . '" class="btn btn-danger"> Desativar</a>';
                            } else {
                                echo '<a href="product/active?id=' . $productId . '" class="btn btn-success"> Ativar</a>';
                            } ?>
                            <a href="product/edit?id=<?php echo $productId ?>" class="btn btn-primary text-white"> Editar</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <div>
                    Nenhum produto cadastro!
                </div>
            <?php endif ?>
        </table>
        <!-- Paginação -->
        <?php if ($page != 1 && $pages > 0): ?>
            <a href='?page=<?php echo($page - 1) ?>' class='btn btn-info'>
                <i class="fas fa-backward"></i> Página Anterior
            </a>
        <?php endif ?>

        <?php if ($page != ($pages - 1)): ?>
            <a href='?page=<?php echo($page + 1) ?>' class='btn btn-info'>Próxima Página <i class="fas fa-forward"></i>
            </a>
        <?php endif ?>
        <!-- Paginação -->
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <!--                    <button type="button" class="close pull-right" data-dismiss="modal" >&times;</button>-->
                    <h4 class="modal-title">Importar produtos através de arquivo CSV</h4>
                </div>
                <div class="modal-body">
                    <form action="../product/savecsv" enctype="multipart/form-data" method="POST">
                        <div class='form-group'>
                            <label for='csv'>Arquivo CSV</label>
                            <input name="csv" id='csv' type="file"/>
                        </div>
                        <button type='submit' class='btn btn-success'><i class="fas fa-file-download"></i> Importar
                        </button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
</div>
    <script>
        <?php include 'assets/required/scripts.js'?>
    </script>
</body>
<footer>
    <div class="footer-image">
        <img src="assets/images/go-jumpers.png" width="119" height="26" alt="Go Jumpers"/>
    </div>
    <div class="email-content">
        <span>wjunior013@gmail.com</span>
    </div>
</footer>

