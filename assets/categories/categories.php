<?php
$categories = $_REQUEST['categories'];
require_once 'assets/required/bootstrap.php';
?>
<div id="mySidenav" class="sidenav text-center">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <a href="/category">Categorias</a>
    <a href="/product">Produtos</a>
    <img src='assets/images/go-logo.png'>
</div>
<header>
    <title>Webjump | Backend Test | Dashboard</title>
    <div class="go-menu">
        <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
        <a href="dashboard.html" class="link-logo">
            <img src="assets/images/go-logo.png" alt="Welcome" width="69" height="430"/></a>
    </div>
</header>
<style>
    <?php include 'assets/css/style.css';?>
</style>
<body class='text-center'>
<div class='card mt-1'>
    <div class='card-body center'>
        <div witdh='100%' class='text-right'>
            <a href='category/create' class='btn btn-success'>+ Cadastrar Nova Categoria</a>
        </div>
        <table class='table table-bordered table-hover mt-2'>
            <tr>
                <th width="50%">Nome</th>
                <th width="25%">Código</th>
                <th width="10%">Ativa?</th>
                <th width="15%">Ações</th>
            </tr>
            <?php foreach ($categories as $category): ?>
                <tr>
                    <td><?php echo $category["name"]; ?></td>
                    <td><?php echo $category["code"]; ?></td>
                    <td>
                        <?php if ($category["active_flag"] == 1) {
                            echo '<span class="badge badge-success"> Sim </span>';
                        } else {
                            echo '<span class="badge badge-danger"> Não </span>';
                        } ?>
                    </td>
                    <td>
                        <?php
                        $categoryId = $category['id'];
                        ?>
                        <?php if ($category["active_flag"] == 1) {
                            echo '<a href="category/delete?id=' . $categoryId . '" class="btn btn-danger"> Desativar</a>';
                        } else {
                            echo '<a href="category/active?id=' . $categoryId . '" class="btn btn-success"> Ativar</a>';
                        } ?>
                        <a href="category/edit?id=<?php echo $categoryId ?>" class="btn btn-primary text-white"> Editar</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
<script>
    <?php include 'assets/required/scripts.js'?>
</script>
</body>
<footer>
    <div class="footer-image">
        <img src="assets/images/go-jumpers.png" width="119" height="26" alt="Go Jumpers"/>
    </div>
    <div class="email-content">
        <span>wjunior013@gmail.com</span>
    </div>
</footer>

