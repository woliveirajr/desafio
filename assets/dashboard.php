<?php
$products = $_REQUEST['products'];
$pages    = $_REQUEST['pages'];
$page     = $_GET["page"];
$url      = $_SERVER["HTTP_ORIGIN"];
if (!$page) {
    $page = 1;
}
if ($pages <= 0) {
    $page = 0;
}
require_once 'required/bootstrap.php';
?>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<script async src="https://cdn.ampproject.org/v0.js"></script>
<style>
    <?php include 'css/style.css';?>
</style>
<header>
    <div class="go-menu">
        <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
        <a href="dashboard.php" class="link-logo">
            <img src="assets/images/go-logo.png" alt="Welcome" width="69" height="430"/>
        </a>
    </div>
</header>
<!-- Header -->
<!-- Main Content -->
<main class="content">
    <div id="mySidenav" class="sidenav text-center">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a href="/category">Categorias</a>
        <a href="/product">Produtos</a>
        <img src='assets/images/go-logo.png'>
    </div>
    <div class="header-list-page">
        <h1 class="title">Loja</h1>
    </div>
    <ul class="product-list">
        <?php if (count($products) > 0): ?>
            <?php foreach ($products as $product): ?>
                <li style="width:250px">
                    <div class="product-image">
                        <?php if (empty($product["product_image_path"])): ?>
                            <td>
                                <img class='productImage' src='<?php echo "assets/images/no-image.jpg"; ?>' layout="responsive" width="164" height="145">
                            </td>
                        <?php else: ?>
                            <td>
                                <img class='productImage' src='<?php echo $product["product_image_path"]; ?>' layout="responsive" width="164" height="145">
                            </td>
                        <?php endif; ?>
                    </div>
                    <div class="product-info">
                        <div class="product-name">
                            <span><?php echo $product["product_name"] ?></span>
                        </div>
                        <div class="product-price">
                            <span class="special-price"><?php ?> available</span>
                            <span><?php echo "R$" . money_format('%i', $product["product_price"]) ?></span>
                        </div>
                        <div>
                            <b>Categorias:</b>
                            <?php foreach ($product["categories"] as $category): ?>
                                <br>
                                <?php echo $category[0]["name"]; ?>
                            <?php endforeach ?>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        <?php else: ?>
            <div>
                Nenhum produto cadastro!
            </div>
        <?php endif ?>
    </ul>
</main>
<!-- Main Content -->
<!-- Footer -->
<footer>
    <div class="footer-image">
        <img src="assets/images/go-jumpers.png" width="119" height="26" alt="Go Jumpers"/>
    </div>
    <div class="email-content">
        <span>wjunior013@gmail.com</span>
    </div>
</footer>
<script>
    <?php include 'assets/required/scripts.js'?>
</script>