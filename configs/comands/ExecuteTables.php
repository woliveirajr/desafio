<?php
require_once 'configs/environment.php';

//onde estão os arquivos de create table
$path = "database/";

//pega todos os arquivos que sejam .php
$diretorio = glob("$path{*.php}", GLOB_BRACE);

$dbServer   = environment::DB_SERVER; // IP do servidor (Desenvolvendo em localhost)
$dbName     = environment::DB_NAME; // Banco
$dbUsername = environment::DB_USERNAME; // Login do banco
$dbPassword = environment::DB_PASSWORD; // Senha do banco

//faz a conexão com o banco sem o database
$conn = new mysqli($dbServer, $dbUsername, $dbPassword, $dbName);

//verifica se não houve algum error com a conexão
if ($conn->connect_error) {
    die("Conexão com o banco de dados falhou: " . $conn->connect_error);
}

foreach ($diretorio as $arquivo) {
    $chekCreate = $conn->query("SELECT table_execute FROM database_tables where table_execute = '" . $arquivo . "';");
    if ($chekCreate->num_rows == 0) {
        $output = shell_exec("php -f $arquivo");
        echo "$output \n";
        $conn->query("INSERT INTO database_tables (table_execute,created_at) values ('" . $arquivo . "','" . date('Y-m-d H:m:i') . "');");
    }
}

$conn->close();