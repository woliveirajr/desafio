<?php

require_once 'model/CsvImports.php';
require_once 'controller/productController.php';

$csvImports = new CsvImports();
$execute    = $csvImports->getAlgumaCoisa();

if (!empty($execute) && isset($execute[0])) {
    $csvData = $csvImports->csvToArray($execute[0]["file_path"]);

    $productController = new productController;
    if ($productController->importCSVFile($csvData)) {
        $dateNow = new DateTime();
        $dateNow = $dateNow->format('Y-m-d H:i:s');
        $csvData = $csvImports->update(
            [
                "id" => $execute[0]["id"],
            ],
            [
                "executed_at" => $dateNow,
            ]
        );
    }
}