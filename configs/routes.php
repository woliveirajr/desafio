<?php

/**
 * Class routes
 * @author Wilson Junior
 */
class routes
{
    /**
     * @var array
     * @description Lista de rotas
     */
    private $routes = [
        "get"  => [
            "/category"        => [
                "controller" => "categoryController",
                "method"     => "index",
            ],
            "/category/create" => [
                "controller" => "categoryController",
                "method"     => "create",
            ],
            "/category/delete" => [
                "controller" => "categoryController",
                "method"     => "delete",
            ],
            "/category/active" => [
                "controller" => "categoryController",
                "method"     => "active",
            ],
            "/category/edit"   => [
                "controller" => "categoryController",
                "method"     => "edit",
            ],
            "/product"         => [
                "controller" => "productController",
                "method"     => "index",
            ],
            "/product/create"  => [
                "controller" => "productController",
                "method"     => "create",
            ],
            "/product/edit"    => [
                "controller" => "productController",
                "method"     => "edit",
            ],
            "/product/delete"  => [
                "controller" => "productController",
                "method"     => "delete",
            ],
            "/product/active"  => [
                "controller" => "productController",
                "method"     => "active",
            ],
            "/dashboard"       => [
                "controller" => "dashboardController",
                "method"     => "index",
            ],
        ],
        "post" => [
            "/category"        => [
                "controller" => "categoryController",
                "method"     => "store",
            ],
            "/category/update" => [
                "controller" => "categoryController",
                "method"     => "update",
            ],
            "/product"         => [
                "controller" => "productController",
                "method"     => "store",
            ],
            "/product/update"         => [
                "controller" => "productController",
                "method"     => "update",
            ],
            "/product/savecsv" => [
                "controller" => "productController",
                "method"     => "saveCSVFile",
            ],
        ],
    ];
    /**
     * @var Array | null
     */
    private $routeData = null;

    /**
     * routes constructor.
     */
    public function __construct()
    {
        $requestUrl = $_SERVER["REQUEST_URI"];
        if (!empty($_POST) || !empty($_FILES)) {
            $routes = $this->routes["post"];
        } else {
            $routes = $this->routes["get"];
        }
        if (!empty($requestUrl)) {
            if (strpos($requestUrl, "?") == true) {
                $requestUrl = explode("?", $requestUrl);
                $requestUrl = $requestUrl[0];
            }
            $routeData = $routes[$requestUrl];
            if (empty($routeData)) {
                $this->routeData = null;
            }

            $this->routeData = $routeData;
        } else {
            $this->routeData = null;
        }
    }

    /**
     * @return array|null
     */
    public function getRouteData()
    {
        return $this->routeData;
    }
}