<?php

require_once 'model/modelTrait.php';

/**
 * Class Product
 * @author Wilson Junior
 */
class Product
{
    use modelTrait;
    /**
     * @var string
     */
    private $name = "products";
    /**
     * @var array
     * @description Colunas que irão trazer do banco
     */
    private $fillable = [
        "id",
        "name",
        "SKU",
        "price",
        "description",
        "image_path",
        "quantity",
        "active_flag",
    ];

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->databaseConnect();
    }

    /**
     * @return void
     * @description Close database connection
     */
    public function closeDBConnection()
    {
        $this->closeConnection();
    }

    /**
     * ...
     * getters e setters
     * ...
     */

    /**
     * @param $data
     * @return bool|string
     */
    public function save($data)
    {
        unset($data["categories"]);
        $data   = $this->getQuerydataByRequest($data);
        $keys   = $data["keys"];
        $values = $data["values"];

        $result = $this->getQueryResult("INSERT INTO $this->name ($keys) VALUES ($values)");
        if ($result) {
            return true;
        } else {
            return false;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getLastInserted()
    {

        try {
            $this->databaseConnect();
            // Attempt select query execution
            $sql            = "SELECT * FROM $this->name ORDER BY id DESC LIMIT 1";
            $databaseResult = mysqli_query($this->databaseConnection, $sql);
            $result         = [];
            while ($row = mysqli_fetch_array($databaseResult)) {
                $aux = [];
                foreach ($this->fillable as $fillable) {
                    $aux[$fillable] = $row[$fillable];
                }
                $result[] = $aux;
            }

            return $result;
        } catch
        (Exception $ex) {
            var_dump($ex->getMessage());
        }
    }

    public
    function update($data, $id)
    {
        unset($data['categories']);
        $data = $this->getQuerydataToUpdate($data);

        if ($this->getQueryResult("UPDATE $this->name SET $data WHERE id = $id")) {
            return true;
        } else {
            return false;
        }

        return false;
    }

    /**
     * @param $active
     * @param int $page
     * @param int $perPage
     * @return array
     */
    public function listAll($active = false, $page = 1, $perPage = 20)
    {
        try {
            $offset = $perPage * $page;
            $sql    = "SELECT p.id as product_id
                                , p.name as product_name
                                , p.SKU as product_SKU
                                , p.price as product_price
                                , p.description as product_description
                                , p.image_path as product_image_path
                                , p.active_flag as product_active_flag
                                , c.id as category_id
                                , c.name as category_name
                                , c.code as category_code
                                FROM products p 
                                LEFT JOIN product_category pc ON pc.product_id = p.id 
                                LEFT JOIN categories c ON pc.category_id = c.id";
            if ($active) {
                $sql = $sql . " where p.active_flag = $active";
            }
            $countSql       = "SELECT COUNT(*) FROM $this->name";
            $databaseResult = mysqli_query($this->databaseConnection, $countSql);
            $countResults   = reset(mysqli_fetch_array($databaseResult));
            $databaseResult = mysqli_query($this->databaseConnection, $sql);
            $result         = [];
            while ($row = mysqli_fetch_array($databaseResult)) {
                $fillableWithRelations = [
                    "product_id",
                    "product_name",
                    "product_SKU",
                    "product_price",
                    "product_description",
                    "product_image_path",
                    "product_active_flag",
                    "category_id",
                    "category_name",
                    "category_code",
                ];

                $aux = [];
                foreach ($fillableWithRelations as $fillable) {
                    $aux[$fillable] = $row[$fillable];
                }
                $result[] = $aux;
            }

            $result          = $this->groupCategories($result);
            $result["pages"] = $countResults / $perPage;

            return $result;
        } catch (Exception $ex) {
            var_dump($ex->getMessage());
        }
    }

    /**
     * @param array $result
     * @return array
     */
    private function groupCategories(array $result)
    {
        $newResult = [];
        foreach ($result as $value) {
            $categoryData = [];
            if (isset($newResult[$value["product_id"]])) {
                $categoryData[]                                  = [
                    "id"   => $value["category_id"],
                    "name" => $value["category_name"],
                    "code" => $value["category_code"],
                ];
                $newResult[$value["product_id"]]["categories"][] = $categoryData;
            } else {
                $categoryData[] = [
                    "id"   => $value["category_id"],
                    "name" => $value["category_name"],
                    "code" => $value["category_code"],
                ];
                unset($value["category_id"]);
                unset($value["category_name"]);
                unset($value["category_code"]);
                $value["categories"][]           = $categoryData;
                $newResult[$value["product_id"]] = $value;
            }
        }

        return $newResult;
    }

    public
    function remove($id)
    {
        $result = $this->getQueryResult("UPDATE $this->name
        SET active_flag = 0
        WHERE id = $id;");
        if ($result === true) {
            return true;
        } else {
            return false;
        }

        return false;
    }

    public
    function active($id)
    {
        $result = $this->getQueryResult("UPDATE $this->name
        SET active_flag = 1
        WHERE id = $id;");
        if ($result === true) {
            return true;
        } else {
            return false;
        }

        return false;
    }

    public function findById($id)
    {
        try {
            $sql            = "SELECT p.id as product_id
                                , p.name as product_name
                                , p.SKU as product_SKU
                                , p.price as product_price
                                , p.description as product_description
                                , p.image_path as product_image_path
                                , p.active_flag as product_active_flag
                                , p.quantity as product_quantity
                                , c.id as category_id
                                , c.name as category_name
                                , c.code as category_code
                                FROM products p 
                                LEFT JOIN product_category pc ON pc.product_id = p.id 
                                LEFT JOIN categories c ON pc.category_id = c.id
                                where p.id = $id";
            $countSql       = "SELECT COUNT(*) FROM $this->name";
            $databaseResult = mysqli_query($this->databaseConnection, $countSql);
            $countResults   = reset(mysqli_fetch_array($databaseResult));
            $databaseResult = mysqli_query($this->databaseConnection, $sql);
            $result         = [];
            while ($row = mysqli_fetch_array($databaseResult)) {
                $fillableWithRelations = [
                    "product_id",
                    "product_name",
                    "product_SKU",
                    "product_price",
                    "product_quantity",
                    "product_description",
                    "product_image_path",
                    "product_active_flag",
                    "category_id",
                    "category_name",
                    "category_code",
                ];

                $aux = [];
                foreach ($fillableWithRelations as $fillable) {
                    $aux[$fillable] = $row[$fillable];
                }
                $result[] = $aux;
            }

            $result = $this->groupCategories($result);

            return end($result);
        } catch (Exception $ex) {
            var_dump($ex->getMessage());
        }
    }
}