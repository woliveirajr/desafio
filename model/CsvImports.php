<?php

require_once 'model/modelTrait.php';

/**
 * Class svImports
 * @author Wilson Junior
 */
class CsvImports
{
    use modelTrait;
    /**
     * @var string
     */
    private $name = "csv_imports";
    /**
     * @var array
     * @description Colunas que irão trazer do banco
     */
    private $fillable = [
        "id",
        "file_path",
        "executed_at",
        "created_at",
        "updated_at",
    ];

    /**
     * CsvImports constructor.
     */
    public function __construct()
    {
        $this->databaseConnect();
    }

    /**
     * @return void
     * @description Close database connection
     */
    public function closeDBConnection()
    {
        $this->closeConnection();
    }

    public function getAlgumaCoisa()
    {
        $this->databaseConnect();
        $sql            = "SELECT * FROM $this->name WHERE executed_at IS NULL ORDER BY id ASC";
        $databaseResult = mysqli_query($this->databaseConnection, $sql);
        $result         = [];
        while ($row = mysqli_fetch_array($databaseResult)) {
            $aux = [];
            foreach ($this->fillable as $fillable) {
                $aux[$fillable] = $row[$fillable];
            }
            $result[] = $aux;
        }

        return $result;
    }

    /**
     * ...
     * getters e setters
     * ...
     */

    /**
     * @param $data
     * @return bool|string
     */
    public function save($data)
    {
        $data   = $this->getQuerydataByRequest($data);
        $keys   = $data["keys"];
        $values = $data["values"];

        $result = $this->getQueryResult("INSERT INTO $this->name ($keys) VALUES ($values)");
        if ($result) {
            return true;
        } else {
            return false;
        }

        return false;
    }

    public function csvToArray($filePath)
    {
        $arrayData = [];
        $csvData   = [];
        // Nome - SKU - descricao - quantidade - preco - categoria
        if (($handle = fopen($filePath, "r")) !== false) {
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                $aux = null;
                foreach ($data as $value) {
                    $aux .= $value;
                }
                $arrayData[] = $aux;
            }
            fclose($handle);
            unset($arrayData[0]);

            foreach ($arrayData as $value) {
                list($name, $SKU, $description, $quantity, $price, $categories) = explode(";", $value);
                $csvData[] = [
                    "name"        => $name,
                    "SKU"         => $SKU,
                    "description" => $description,
                    "quantity"    => $quantity,
                    "price"       => (float) $price,
                    "categories"  => explode("|", $categories),
                ];
            }
        }

        return $csvData;
    }

    /**
     * @return array
     */
    public function getLastInserted()
    {

        try {
            $this->databaseConnect();
            // Attempt select query execution
            $sql            = "SELECT * FROM $this->name ORDER BY id DESC LIMIT 1";
            $databaseResult = mysqli_query($this->databaseConnection, $sql);
            $result         = [];
            while ($row = mysqli_fetch_array($databaseResult)) {
                $aux = [];
                foreach ($this->fillable as $fillable) {
                    $aux[$fillable] = $row[$fillable];
                }
                $result[] = $aux;
            }

            return $result;
        } catch
        (Exception $ex) {
            var_dump($ex->getMessage());
        }
    }

    /**
     * @param array $search
     * @param array $dataUpdate
     */
    public function update(array $search, array $dataUpdate)
    {
        $arrayLastKey = end(array_keys($dataUpdate));
        $setValues    = null;
        foreach ($dataUpdate as $key => $value) {
            if ($key == $arrayLastKey) {
                $setValues .= $key . " = '" . $value . "'";
            } else {
                $setValues .= $key . " = '" . $value . "',";
            }
        }

        $setSearch = null;
        foreach ($search as $key => $value) {
            $setSearch .= "AND " . $key . " = " . $value;
        }

        $sql = "UPDATE $this->name SET $setValues WHERE 1=1 $setSearch";

        $result = $this->getQueryResult($sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function destroy($id)
    {
        $this->databaseConnect();
        $sql            = "DELETE FROM $this->name WHERE id = $id";
        $databaseResult = mysqli_query($this->databaseConnection, $sql);
        if ($databaseResult) {
            return true;
        }

        return false;
    }

    /**
     *
     */
    public function listAll()
    {
        try {

        } catch (Exception $ex) {
            var_dump($ex->getMessage());
        }
    }
}

?>