<?php

require_once 'model/modelTrait.php';

/**
 * Class Category
 * @author Wilson Junior
 */
class Category
{
    use modelTrait;
    /**
     * @var string
     */
    private $name = "categories";
    /**
     * @var array
     * @description Colunas que irão trazer do banco
     */
    private $fillable = [
        "id",
        "name",
        "code",
        "active_flag",
    ];

    /**
     * Category constructor.
     */
    public function __construct()
    {
        $this->databaseConnect();
    }

    /**
     * @return void
     * @description Close database connection
     */
    public function closeDBConnection()
    {
        $this->closeConnection();
    }

    /**
     * ...
     * getters e setters
     * ...
     */

    /**
     * @param $data
     * @return bool|string
     */
    public function save($data)
    {
        $data   = $this->getQuerydataByRequest($data);
        $keys   = $data["keys"];
        $values = $data["values"];

        if ($this->getQueryResult("INSERT INTO $this->name ($keys) VALUES ($values)")) {
            return true;
        } else {
            return false;
        }

        return false;
    }

    public
    function update($data, $id)
    {
        $data = $this->getQuerydataToUpdate($data);

        if ($this->getQueryResult("UPDATE $this->name SET $data WHERE id = $id")) {
            return true;
        } else {
            return false;
        }

        return false;
    }

    public
    function remove($id)
    {
        $result = $this->getQueryResult("UPDATE categories
        SET active_flag = 0
        WHERE id = $id;");
        if ($result === true) {
            return true;
        } else {
            return false;
        }

        return false;
    }

    public
    function active($id)
    {
        $result = $this->getQueryResult("UPDATE categories
        SET active_flag = 1
        WHERE id = $id;");
        if ($result === true) {
            return true;
        } else {
            return false;
        }

        return false;
    }

    /**
     * @param array $categories
     * @return array
     */
    public function findByName(array $categories)
    {
        $resultCategories = [];
        if (is_array($categories)) {
            foreach ($categories as $category) {
                if (empty($category)) {
                    continue;
                }
                $category       = str_replace("'", "", $category);
                $sql            = "SELECT * FROM $this->name WHERE name = '$category'";
                $databaseResult = mysqli_query($this->databaseConnection, $sql);
                while ($row = mysqli_fetch_array($databaseResult)) {
                    $aux = [];
                    foreach ($this->fillable as $fillable) {
                        $aux[$fillable] = $row[$fillable];
                    }
                    $resultCategories[] = $aux;
                }
            }

            return $resultCategories;
        }
    }

    /**
     * @param array $categoriesIds
     * @return array
     */
    public
    function findByIds(array $categoriesIds)
    {
        $resultCategories = [];
        if (is_array($categoriesIds)) {
            foreach ($categoriesIds as $categoryId) {
                $sql            = "SELECT * FROM $this->name WHERE id = '$categoryId'";
                $databaseResult = mysqli_query($this->databaseConnection, $sql);
                while ($row = mysqli_fetch_array($databaseResult)) {
                    $aux = [];
                    foreach ($this->fillable as $fillable) {
                        $aux[$fillable] = $row[$fillable];
                    }
                    $resultCategories[] = $aux;
                }
            }
        }

        return $resultCategories;
    }

    /**
     * @param string $code
     * @return array
     */
    public
    function findByCode($code)
    {
        $sql              = "SELECT * FROM $this->name WHERE code = '$code'";
        $databaseResult   = mysqli_query($this->databaseConnection, $sql);
        $resultCategories = [];
        while ($row = mysqli_fetch_array($databaseResult)) {
            $aux = [];
            foreach ($this->fillable as $fillable) {
                $aux[$fillable] = $row[$fillable];
            }
            $resultCategories[] = $aux;
        }

        return end($resultCategories);
    }

    function findById($id)
    {
        $sql              = "SELECT * FROM $this->name WHERE id = $id";
        $databaseResult   = mysqli_query($this->databaseConnection, $sql);
        $resultCategories = [];
        while ($row = mysqli_fetch_array($databaseResult)) {
            $aux = [];
            foreach ($this->fillable as $fillable) {
                $aux[$fillable] = $row[$fillable];
            }
            $resultCategories[] = $aux;
        }

        return end($resultCategories);
    }

    /**
     * @param bool $active
     * @return array
     */
    public
    function listAll($active = false)
    {
        try {
            // Attempt select query execution
            $sql = "SELECT * FROM $this->name";
            if ($active) {
                $sql = $sql . " where active_flag = $active";
            }
            $databaseResult = mysqli_query($this->databaseConnection, $sql);
            $result         = [];
            while ($row = mysqli_fetch_array($databaseResult)) {
                $aux = [];
                foreach ($this->fillable as $fillable) {
                    $aux[$fillable] = $row[$fillable];
                }
                $result[] = $aux;
            }

            return $result;
        } catch (Exception $ex) {
            var_dump($ex->getMessage());
        }
    }
    /**
     * ...
     * outros métodos de abstração de banco
     * ...
     */
}
